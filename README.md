Role Name
=========

Downloads minishift and installs the binary.

Requirements
------------

Role Variables
--------------


Dependencies
------------


Example Playbook
----------------

    - hosts: all
      roles:
         - minishift

License
-------

GPLv3

Author Information
------------------
jlozada2426@protonmail.com
